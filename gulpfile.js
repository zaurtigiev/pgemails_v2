var gulp = require('gulp');
const { series } = require('gulp');
const mjml = require('gulp-mjml')
const folder = 'P&G_Gifting';
var webserver = require('gulp-webserver');

gulp.task('mjml', function(cb) {
  gulp.src('./'+folder+'/index.mjml')
      .pipe(mjml())
      .pipe(gulp.dest('./'+folder+'/'));
  cb();
});

gulp.task('build', gulp.series('mjml'));

gulp.task('webserver', gulp.series('build', function(cb){
  gulp.src('./'+folder+'/')
      .pipe(webserver({
          fallback:   './'+folder+'/index.html',
          livereload: true,
          directoryListing: {
              enable: true,
              path: 'public'
          },
          open: true
      }));
  cb();
}));

gulp.task('watch', gulp.series('webserver', function(cb){
  gulp.watch('./'+folder+'/*.mjml', gulp.series('mjml'));
  cb();
}));


exports.default = gulp.series('watch');